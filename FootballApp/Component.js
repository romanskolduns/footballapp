sap.ui.define([
	"sap/ui/core/UIComponent",
	"sap/ui/Device",
	"FootballApp/model/models"
], function(UIComponent, Device, models) {
	"use strict";

	return UIComponent.extend("FootballApp.Component", {

		metadata: {
            manifest: "json",
        },
		init: function() {
			// call the base component's init function
			UIComponent.prototype.init.apply(this, arguments);
            this.getRouter().initialize();
            this.setModel(models.createDeviceModel(), "device");
        }
	});
});