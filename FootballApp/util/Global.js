sap.ui.define([], function() {
	"use strict";
	
	return {
		oBrutalPlayerTop: null,
		oFootballData: null, // original data from backend
		oGameData: null,
		oGoalKeeperData: null,
		oJudgeData: null,
		oJudgeTop: null,
		oPenaltyData: null,
		oPlayerData: null,
		oPlayerTop: null,
		oTeamData: null
	};

});