jQuery.sap.require("jquery.sap.storage");

sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"FootballApp/model/models",
	"FootballApp/util/Global",
    "sap/ui/core/routing/History"
], function(Controller, models, Global, History) {
	"use strict";

	return Controller.extend("FootballApp.controller.Main", {
		global: Global,
		models: models,
		onInit: function() {
			var oViewModel = new sap.ui.model.json.JSONModel();
			var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
			this.global.oFootballData = oStorage.get("footballGameData") || [];
			this.processFootballData();

			if (this.global.oFootballData.length == 0) {
				var oRouter = this.getOwnerComponent().getRouter()
				oRouter.navTo('Upload');
        	}
			oViewModel.setData(this.global);
            sap.ui.getCore().setModel(oViewModel,"globalModel");
			this.getView().setModel(oViewModel);
			oViewModel.refresh(true);
		},
		
		onAfterRendering: function() {
			this.setupFileDrop();
		},
        extPointCheckFileExtension:null,
        extPointConvertToJson:null,
        extPointErrorMessage:null,
		setupFileDrop: function() {
			var dropZone = document.body;
			var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
			var oController = this;
			dropZone.addEventListener('dragover', function(oDragEvent) {
				oDragEvent.stopPropagation();
				oDragEvent.preventDefault();
				oDragEvent.dataTransfer.dropEffect = 'copy';
			});
			dropZone.addEventListener('drop', function(oDropEvent) {
				oDropEvent.stopPropagation();
				oDropEvent.preventDefault();
				var aFiles = oDropEvent.dataTransfer.files;
				var counter = 0;
				for (var i = 0; i < aFiles.length; i++) {
					var fileExt = aFiles[i].name.split(".")[1];
					var extraFileExt = false;
					if (oController.extPointCheckFileExtension) {
						extraFileExt = oController.extPointCheckFileExtension(fileExt);
					}
					if (fileExt === "json" || extraFileExt) {
						var reader = new FileReader();
						reader.onloadend = function() {
                            counter++;
                            var oGameData;
                            try {
                                if (fileExt === 'json') {
                                    oGameData = JSON.parse(this.result);
                                } else if (oController.extPointConvertToJson) {
                                    oGameData = oController.extPointConvertToJson(this.result,fileExt)
                                }

                            } catch (err) {

                            	if (oController.extPointErrorMessage) {
                                    oController.extPointErrorMessage('ERROR in file " + aFiles[i].name + " reader ' + err.toString());
								} else {
                                    console.error('ERROR in file "%s" reader',aFiles[i].name, err.toString());
								}
							}

							try {
                                var oFootballData = oController.global.oFootballData;
                                if (oGameData && oGameData.Spele) {
                                    if (!oFootballData) {
                                        oFootballData = [oGameData];
                                    } else {
                                        var bGameExists = false;
                                        for (var i = 0; i < oFootballData.length; i++) {
                                            if (oFootballData[i].Spele.Laiks === oGameData.Spele.Laiks && oFootballData[i].Spele.Skatitaji === oGameData.Spele.Skatitaji && oFootballData[i].Spele.Vieta === oGameData.Spele.Vieta) {
                                                bGameExists = true;
                                            }
                                        }
                                        if (!bGameExists || !oFootballData.length) {
                                            oFootballData.push(oGameData);
                                        }
                                    }
                                }
							} catch (err) {
                                if (oController.extPointErrorMessage) {
                                    oController.extPointErrorMessage('ERROR in data " + aFiles[i].name + " procesing ' + err.toString());
                                } else {
                                    console.error('ERROR in data "%s" procesing',aFiles[i].name, err.toString());
                                }
							}


							if (counter === aFiles.length) {
								oStorage.put("footballGameData", oFootballData);
								oController.processFootballData();
							}
						};
						
						reader.readAsText(aFiles[i]);
					}
				}
			}.bind(this));
		},
        onNavBack : function () {
            var sPreviousHash = History.getInstance().getPreviousHash();

            //The history contains a previous entry
            if (sPreviousHash !== undefined) {
                window.history.go(-1);
            } else {
                // There is no history!
                // replace the current hash with order id 0 (will not add an history entry)
                //this.getOwnerComponent().getRouter()
                //    .navTo("orderDetails",
                //        {orderId:0}, !Device.system.phone);
                //
            }
        },
		// ---------- Data processing
		processFootballData: function() {
			if (!this.global.oFootballData.length) {
				return;
			}
		
			this.models.processFootballData();

			if (this.getView().getModel()) {
				this.getView().getModel().refresh(true);
			}
		},

		// ---------- Data processing end
		changeTab: function(oEvent) {
			var sTabId  = oEvent.getSource().getInfo();
			var oRouter = this.getOwnerComponent().getRouter()

			oRouter.navTo(sTabId);
		},
        onUploadData:function (oEvent) {
            var oRouter = this.getOwnerComponent().getRouter()
            oRouter.navTo('Upload');
        },
		onTeamNamePress: function(oEvent) {
			var oTeam = this.global.oTeamData.find(x => x.Nosaukums === oEvent.getSource().getText()) || null;
			if (!oTeam) {
				return;
			}
			var oListItemTemplate = new sap.m.StandardListItem({
				title: {parts: [
						{ path: "Vards" },
						{ path: "Uzvards" },
						{ path: "Nr" },
						{ path: "Loma" }
					],
					formatter: function(sName, sSurname, iNr, sRole) {
						return sName + " " + sSurname + " (Nr " + iNr + ", " + this.formatPlayerRole(sRole) + ")";
					}.bind(this)
				}
			});
			var oTeamModel = new sap.ui.model.json.JSONModel();
			oTeamModel.setData(oTeam.Pamatsastavs.Speletajs);
			var oPopover = new sap.m.Popover({
				showHeader: false,
				placement: "Bottom",
				content: [
					new sap.m.VBox({
						items: [
							new sap.m.Title({text: oTeam.Nosaukums, titleStyle: "H3"}).addStyleClass("sapUiSmallMarginBegin sapUiSmallMarginTopBottom"),
							new sap.m.Title({text: oTeam.Pamatsastavs.Speletajs.length + " players:", titleStyle: "H5"}).addStyleClass("sapUiSmallMarginBegin"),
							new sap.m.List({
								noDataText: "",
								items: []
							}).bindItems("/", oListItemTemplate).setModel(oTeamModel)
						]
					}).addStyleClass("sapUiSmallMarginBeginEnd")
				]
			});
			oPopover.openBy(oEvent.getSource());
		},
		
		onClearAllPress: function() {
            var oRouter = this.getOwnerComponent().getRouter()
            oRouter.navTo('Upload');
			jQuery.sap.storage(jQuery.sap.storage.Type.local).put("footballGameData", "");
			location.reload();
		},
		
		formatTime: function(sTime) {
			var aMonths = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
			sTime = sTime.split("/");
			sTime[1] = parseInt(sTime[1], 10) - 1;
			var dDate = new Date(sTime[0], sTime[1], sTime[2]);
			return aMonths[dDate.getMonth()] + " " + dDate.getDate() + ", " + dDate.getFullYear();
		},
		
		formatPlayerRole: function(sRole) {
			switch (sRole) {
				case "V" : sRole = "Goalkeeper"; break;
				case "A" : sRole = "Defender"; break;
				case "U" : sRole = "Forward"; break;
			}
			return sRole;
		}
	});
});