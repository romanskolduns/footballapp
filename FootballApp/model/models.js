sap.ui.define([
	"sap/ui/model/json/JSONModel",
	"sap/ui/Device",
	"FootballApp/util/Global"
], function(JSONModel, Device, Global) {
	"use strict";

	return {
		global: Global,
		createDeviceModel: function() {
			var oModel = new JSONModel(Device);
			oModel.setDefaultBindingMode("OneWay");
			return oModel;
		},
		
		loadFootballData: function(oRequests, fnSuccess, fnError) {
			if (!oRequests) {
				return;
			}
			var aRequests = [];
			if (oRequests instanceof Array === false) {
				if (oRequests.hasOwnProperty("start") && oRequests.hasOwnProperty("end") && oRequests.start < oRequests.end) {
					for (var i = oRequests.start; i <= oRequests.end; i++) {
						aRequests.push(i);
					}
				}
			} else {
				aRequests = oRequests;
			}
			
			var oFootballData = [];
			var counter = 0;
			for (var i = 0; i < aRequests.length; i++) {
				$.ajax({
					type: "GET",
					url: "model/futbols" + aRequests[i] + ".json",
					success: function(oEvent) {
						counter++;
						oFootballData.push(JSON.parse(oEvent));
						if (counter === aRequests.length) {
							fnSuccess(oFootballData);
						}
					},
					error: function(oEvent) {
						fnError(oEvent);
					}
				});
			}
		},

		processFootballData: function() {
			var i, j, k;
			this.global.oGameData = this.global.oFootballData.map(x => x.Spele);
			this.prepareGameData(this.global.oGameData);
			var oGameData = jQuery.extend(true, [], this.global.oGameData);
			
			this.global.oPenaltyData = [];
			for (i = 0; i < oGameData.length; i++) {
				for (j = 0; j < oGameData[i].Komanda.length; j++) {
					if (oGameData[i].Komanda[j].Sodi.Sods.length) {
						for (k = 0; k < oGameData[i].Komanda[j].Sodi.Sods.length; k++) {
							this.global.oPenaltyData.push(oGameData[i].Komanda[j].Sodi.Sods[k]);
						}
					}
				}
			}
			
			this.global.oTeamData = oGameData.map(x => x.Komanda);
			this.global.oTeamData = [].concat(...this.global.oTeamData);
			this.global.oTeamData = this.global.oTeamData.filter((obj, pos, arr) => {
				return arr.map(x => x["Nosaukums"]).indexOf(obj["Nosaukums"]) === pos;
			});
			this.global.oTeamData.forEach(x => {x.Punkti = 0; x.SpeluSkaits = 0; x.UzvaruSkaits = 0; x.ZaudejumuSkaits = 0; x.Varti.Skaits = 0; x.Sodi.Skaits = 0;});
			this.processTeamData();
			
			this.global.oPlayerData = this.global.oTeamData.map(x => x.Speletaji.Speletajs);
			this.global.oPlayerData = [].concat(...this.global.oPlayerData);
			this.global.oPlayerData = this.global.oPlayerData.filter((obj, pos, arr) => {
				return arr.map(x => x["Nr"]).indexOf(obj["Nr"]) === pos;
			});
			var oPlayer = {};
			this.global.oPlayerData.forEach(x => {if (x.Loma !== "V") {x.Varti = {Skaits: 0}; x.Piespeles = {Skaits: 0}}});
			for (i = 0; i < oGameData.length; i++) {
				for (j = 0; j < oGameData[i].Komanda.length; j++) {
					if (oGameData[i].Komanda[j].Varti.VG.length) {
						for (k = 0; k < oGameData[i].Komanda[j].Varti.VG.length; k++) {
							oPlayer = this.global.oPlayerData.find(x => x.Nr === oGameData[i].Komanda[j].Varti.VG[k].Nr) || null;
							if (oPlayer && oPlayer.Loma !== "V") {
								oPlayer.Varti.Skaits += 1;
							}
							for (var l = 0; l < oGameData[i].Komanda[j].Varti.VG[k].P.length; l++) {
								oPlayer = this.global.oPlayerData.find(x => x.Nr === oGameData[i].Komanda[j].Varti.VG[k].P[l].Nr) || null;
								if (oPlayer && oPlayer.Loma !== "V") {
									oPlayer.Piespeles.Skaits += 1;
								}
							}
						}
					}
				}
			}
			
			var aPenalties = [];
			for (i = 0; i < this.global.oPlayerData.length; i++) {
				if (this.global.oPlayerData[i].Loma !== "V") {
					this.global.oPlayerData[i].Sodi = {Skaits: 0};
					aPenalties = this.global.oPenaltyData.filter(x => x.Nr === this.global.oPlayerData[i].Nr) || [];
					this.global.oPlayerData[i].Sodi.Skaits += aPenalties.length;
				}
			}
			
			this.global.oJudgeData = oGameData.map(x => x.T);
			var oSeniorJudgeData = oGameData.map(x => x.VT);
			this.global.oJudgeData = [].concat(...this.global.oJudgeData);
			this.global.oJudgeData = this.global.oJudgeData.concat(oSeniorJudgeData);
			this.global.oJudgeData = this.global.oJudgeData.filter((obj, pos, arr) => {
				return arr.map(x => x["Uzvards"]).indexOf(obj["Uzvards"]) === pos;
			});
			this.global.oJudgeData.forEach(x => {x.SoduSkaits = 0; x.SpeluSkaits = 0; x.VidejiSodi = 0;});
			this.global.oGoalKeeperData = [];
			
			var oTeam1, oTeam2, oJudge, oJudge1, oJudge2, aJudges, aGoalKeepers;
			for (i = 0; i < oGameData.length; i++) {
				oJudge = this.global.oJudgeData.find(x => x.Uzvards === oGameData[i].VT.Uzvards) || null;
				oJudge1 = this.global.oJudgeData.find(x => x.Uzvards === oGameData[i].T[0].Uzvards) || null;
				oJudge2 = this.global.oJudgeData.find(x => x.Uzvards === oGameData[i].T[1].Uzvards) || null;
				aJudges = [oJudge, oJudge1, oJudge2];
				if (oGameData[i].Komanda.length === 2) {
					oTeam1 = this.global.oTeamData.find(x => x.Nosaukums === oGameData[i].Komanda[0].Nosaukums) || null;
					oTeam2 = this.global.oTeamData.find(x => x.Nosaukums === oGameData[i].Komanda[1].Nosaukums) || null;
					if (oTeam1 && oTeam2 && oJudge && oJudge1 && oJudge2) {
						aJudges.forEach(x => {
							x.SoduSkaits += oGameData[i].Komanda[0].Sodi.Sods.length;
							x.SoduSkaits += oGameData[i].Komanda[1].Sodi.Sods.length;
							x.SpeluSkaits++;
							x.VidejiSodi = parseFloat((x.SoduSkaits / x.SpeluSkaits).toFixed(1), 10);
						});
					}
					
					aGoalKeepers = oTeam1.Speletaji.Speletajs.filter(x => x.Loma === "V") || null;
					if (aGoalKeepers) {
						this.global.oGoalKeeperData = this.global.oGoalKeeperData.concat(aGoalKeepers);
					}
					aGoalKeepers = oTeam2.Speletaji.Speletajs.filter(x => x.Loma === "V") || null;
					if (aGoalKeepers) {
						this.global.oGoalKeeperData = this.global.oGoalKeeperData.concat(aGoalKeepers);
					}
				}
			}
			
			this.global.oGoalKeeperData = this.global.oGoalKeeperData.filter((obj, pos, arr) => {
				return arr.map(x => x["Uzvards"]).indexOf(obj["Uzvards"]) === pos;
			});
			this.global.oGoalKeeperData = jQuery.extend(true, [], this.global.oGoalKeeperData);
			this.global.oGoalKeeperData.forEach(x => {x.SpeluSkaits = 0; x.VartuSkaits = 0; x.VidejaisVartuSkaits = 0;});
			var aGoalKeepers, aTeams;
			for (i = 0; i < oGameData.length; i++) {
				if (oGameData[i].Komanda.length === 2) {
					aTeams = oGameData[i].Komanda;
					if (aTeams[0] && aTeams[1]) {
						for (j = 0; j < aTeams.length; j++) {
							aGoalKeepers = aTeams[j].Speletaji.Speletajs.filter(x => x.Loma === "V") || null;
							if (aGoalKeepers) {
								aGoalKeepers.forEach(x => {
									var oGoalKeeper = this.global.oGoalKeeperData.find(g => g.Uzvards === x.Uzvards) || null;
									if (oGoalKeeper) {
										oGoalKeeper.SpeluSkaits++;
										oGoalKeeper.VartuSkaits += aTeams[j].Varti.Skaits;
										oGoalKeeper.VidejaisVartuSkaits = parseFloat((oGoalKeeper.VartuSkaits / oGoalKeeper.SpeluSkaits).toFixed(1), 10);
									}
								});
							}
						}
					}
				}
			}
			this.global.oGoalKeeperData.sort((a, b) => {
				return (a.VidejaisVartuSkaits === b.VidejaisVartuSkaits) ? 0 : (a.VidejaisVartuSkaits > b.VidejaisVartuSkaits) ? -1 : 1;
			});
			this.global.oGoalKeeperData = this.global.oGoalKeeperData.slice(0, 5);
			
			this.global.oBrutalPlayerTop = jQuery.extend(true, [], this.global.oPlayerData);
			this.global.oBrutalPlayerTop = this.global.oBrutalPlayerTop.filter(x => x.Loma !== "V");
			this.global.oBrutalPlayerTop.sort((a, b) => {
				return (a.Sodi.Skaits === b.Sodi.Skaits) ? 0 : (a.Sodi.Skaits > b.Sodi.Skaits) ? -1 : 1;
			});
			this.global.oBrutalPlayerTop = this.global.oBrutalPlayerTop.slice(0, 10);
			
			this.global.oPlayerTop = jQuery.extend(true, [], this.global.oPlayerData);
			this.global.oPlayerTop = this.global.oPlayerTop.filter(x => x.Loma !== "V");
			this.sortPlayerData();
			this.global.oPlayerTop = this.global.oPlayerTop.slice(0, 10);
			
			this.global.oJudgeTop = jQuery.extend(true, [], this.global.oJudgeData);
			this.global.oJudgeTop.sort((a, b) => {
				return (a.VidejiSodi === b.VidejiSodi) ? 0 : (a.VidejiSodi > b.VidejiSodi) ? -1 : 1;
			});
			this.global.oJudgeTop = this.global.oJudgeTop.slice(0, 10);
			
			this.global.oTeamData.sort((a, b) => {
				return (a.Punkti === b.Punkti) ? 0 : (a.Punkti > b.Punkti) ? -1 : 1;
			});
			for (i = 0; i < this.global.oTeamData.length; i++) {
				this.global.oTeamData[i].Vieta = i + 1;
			}

			for (i = 0; i < 10; i++) {
				if (i < this.global.oBrutalPlayerTop.length) {
					this.global.oBrutalPlayerTop[i].Vieta = i + 1;
				}
				if (i < this.global.oPlayerTop.length) {
					this.global.oPlayerTop[i].Vieta = i + 1;
				}
				if (i < this.global.oJudgeTop.length) {
					this.global.oJudgeTop[i].Vieta = i + 1;
				}
				if (i < this.global.oGoalKeeperData.length) {
					this.global.oGoalKeeperData[i].Vieta = i + 1;
				}
			}
			if (this.extPointProcessData) {
                this.extPointProcessData()
			}
		},
		extPointProcessData:null,
		prepareGameData: function(oGameData) {
			var oKomanda, i, j;
			for (i = 0; i < oGameData.length; i++) {
				for (j = 0; j < oGameData[i].Komanda.length; j++) {
					oKomanda = oGameData[i].Komanda[j];
					if (!oKomanda.hasOwnProperty("Sodi") || typeof oKomanda.Sodi === "string") {
						oKomanda.Sodi = {Skaits: 0, Sods: []};
					}
					if (!oKomanda.hasOwnProperty("Varti") || typeof oKomanda.Varti === "string") {
						oKomanda.Varti = {Skaits: 0, VG: []};
					}
					
					if ((typeof oKomanda.Sodi.Sods === "object") && (oKomanda.Sodi.Sods instanceof Array === false)) {
						oKomanda.Sodi.Sods = [oKomanda.Sodi.Sods];
					}
					oKomanda.Sodi.Skaits = oKomanda.Sodi.Sods.length;
					
					if ((typeof oKomanda.Varti.VG === "object") && (oKomanda.Varti.VG instanceof Array === false)) {
						oKomanda.Varti.VG = [oKomanda.Varti.VG];
					}
					oKomanda.Varti.Skaits = oKomanda.Varti.VG.length;
					
					for (var k = 0; k < oKomanda.Varti.VG.length; k++) {
						if (!oKomanda.Varti.VG[k].hasOwnProperty("P") || !oKomanda.Varti.VG[k].P) {
							oKomanda.Varti.VG[k].P = [];
						}
						if ((typeof oKomanda.Varti.VG[k].P === "object") && (oKomanda.Varti.VG[k].P instanceof Array === false)) {
							oKomanda.Varti.VG[k].P = [oKomanda.Varti.VG[k].P];
						}
					}
				}
			}
		},
		processTeamData: function() {
			var oGameData = this.global.oGameData;
			var oTeamData = this.global.oTeamData;
			var oTeam = {};
			var oTeam1 = {};
			var oTeam2 = {};
			for (var j = 0; j < oGameData.length; j++) {
				for (var k = 0; k < oGameData[j].Komanda.length; k++) {
					oTeam = oTeamData.find(x => x.Nosaukums === oGameData[j].Komanda[k].Nosaukums) || null;
					if (oTeam) {
						oTeam.SpeluSkaits++;
						oTeam.Varti.Skaits += oGameData[j].Komanda[k].Varti.Skaits;
						oTeam.Sodi.Skaits += oGameData[j].Komanda[k].Sodi.Skaits;
					}
				}
				if (oGameData[j].Komanda.length === 2) {
					oTeam1 = oTeamData.find(x => x.Nosaukums === oGameData[j].Komanda[0].Nosaukums) || null;
					oTeam2 = oTeamData.find(x => x.Nosaukums === oGameData[j].Komanda[1].Nosaukums) || null;
					if (oTeam1 && oTeam2) {
						if (oGameData[j].Komanda[0].Varti.Skaits > oGameData[j].Komanda[1].Varti.Skaits) {
							oTeam1.Punkti += 5;
							oTeam2.Punkti += 1;
							oTeam1.UzvaruSkaits += 1;
							oTeam2.ZaudejumuSkaits += 1;
						} else {
							oTeam1.Punkti += 1;
							oTeam2.Punkti += 5;
							oTeam1.ZaudejumuSkaits += 1;
							oTeam2.UzvaruSkaits += 1;
						}
					}
				}
			}
			var oPlayer = {};
			for (var i = 0; i < oTeamData.length; i++) {
				for (var j = 0; j < oTeamData[i].Pamatsastavs.Speletajs.length; j++) {
					oPlayer = oTeamData[i].Speletaji.Speletajs.find(x => x.Nr === oTeamData[i].Pamatsastavs.Speletajs[j].Nr) || null;
					if (oPlayer) {
						oTeamData[i].Pamatsastavs.Speletajs[j].Loma = oPlayer.Loma;
						oTeamData[i].Pamatsastavs.Speletajs[j].Vards = oPlayer.Vards;
						oTeamData[i].Pamatsastavs.Speletajs[j].Uzvards = oPlayer.Uzvards;
					}
				}
			}
		},
		sortPlayerData: function() {
			this.global.oPlayerTop.sort((a, b) => {
				if (a.Varti.Skaits > b.Varti.Skaits) {
					return -1;
				}
				if (a.Varti.Skaits < b.Varti.Skaits) {
					return 1;
				}
				
				if (a.Piespeles.Skaits > b.Piespeles.Skaits) {
					return -1;
				}
				if (a.Piespeles.Skaits < b.Piespeles.Skaits) {
					return 1;
				}
				return 0;
			});
		},
		
		// How to use loadFootballData in controller onInit:
		/*this.byId("detailContainer").setBusy(true);
			var errCount = 0;
			models.loadFootballData(
				{start: 0, end: 5},
				function(oFootballData) {
					this.byId("detailContainer").setBusy(false);
					this.global.oFootballData = oFootballData;
					this.processFootballData();
					oViewModel.setData(this.global);
					this.getView().setModel(oViewModel);
				}.bind(this),
				function(oEvent) {
					if (!errCount) {
						this.byId("detailContainer").setBusy(false);
						new sap.m.MessageToast.show("Could not load football data");
					}
					errCount++;
				}.bind(this)
			);*/

	};
});